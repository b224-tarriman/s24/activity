const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`)

const address = ["Tabok","Mandaue City","Cebu"]

const [brgy,city,prov] = address

console.log(`I live at ${brgy} ${city}, ${prov}.`)

let animal ={
		name:"Lolong",
		type:"saltwater crocodile",
		weight: 1075,
		height: 20,
		width: 3
}

const {name,type,weight,height,width} = animal

console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${height} ft ${width} in.`)

const number = [1,2,3,4,5,15]

number.forEach((num)=>console.log(num))

class Dog{
	constructor(name,age,breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const shihTzu = new Dog("Zyla",1,"Shih Tzu")

console.log(shihTzu)